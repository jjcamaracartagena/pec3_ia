﻿using UnityEngine;

namespace CocheAgentes
{

    public class KeyboardInput : BaseInput
    {
        
         string AccelerateButtonName = "Accelerate";
         string BrakeButtonName = "Brake";

        public override InputData GenerateInput() {
            return new InputData
            {
                Accelerate = Input.GetButton(AccelerateButtonName),
                Brake = Input.GetButton(BrakeButtonName),
                TurnInput = Input.GetAxis("Horizontal")
            };
        }
    }
}
