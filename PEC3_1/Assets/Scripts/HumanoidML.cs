﻿using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class HumanoidML : Agent
{
    [Header("Velocidad")]
    [Range(0f, 5f)]
    public float _speed;

    [Header("Velocidad de giro")]
    [Range(50f, 300f)]
    public float _turnSpeed;

    public bool _training = true;


    protected Rigidbody _rb;

    [SerializeField]
    protected Transform _target;
    protected Animator _anim;

    protected Vector3 _previous;
    RandomPointOnNavMesh newPoint;

 
    public override void Initialize()
    {
        _rb = GetComponent<Rigidbody>();
        _anim = GetComponent<Animator>();

        _previous = this.transform.position;
        newPoint = _target.gameObject.GetComponent<RandomPointOnNavMesh>();

        //MaxStep forma parte de la clase Agent
        if (!_training) MaxStep = 0;
    }

    public override void OnEpisodeBegin()
    {

       this.transform.position = _previous;
        this.transform.rotation = Quaternion.identity;

        _rb.velocity = Vector3.zero;
        _rb.angularVelocity = Vector3.zero;

        // Se cambia la posicion del target
        newPoint.ChangePosition();


    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float lForward = actions.ContinuousActions[0];
        float lTurn = 0;
        if (actions.ContinuousActions[1] == 1)
        {
            lTurn = -1;
        }
        else if (actions.ContinuousActions[1] == 2)
        {
            lTurn = 1;
        }

        _rb.MovePosition(transform.position + transform.forward * lForward * _speed * Time.deltaTime);

        transform.Rotate(transform.up * lTurn * _turnSpeed * Time.deltaTime);

        // Si se cae, se penaliza con -1
        if (this.transform.localPosition.y < 0)
        {
            AddReward(-1f);
            EndEpisode();
        }

    }

    public override void CollectObservations(VectorSensor sensor1)
    {
        //Distancia al target.
        //Vector 1 posicion.
        sensor1.AddObservation(Vector3.Distance(_target.transform.position, transform.position));

        //Dirección al target.
        //Vector 3 posiciones. 
        sensor1.AddObservation((_target.transform.position - transform.position).normalized);

        //Vector del señor, donde mira.
        //Vector de 3 posiciones. 
        sensor1.AddObservation(transform.forward);



    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {

        var continuousActionsOut = actionsOut.ContinuousActions;

        float lForward = 0f;
        float lTurn = 0f;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            lForward = 1f;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            lTurn = 1f;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            lTurn = 2f;
        }else if (Input.GetKey(KeyCode.DownArrow)) { lForward = -1f; }

        // Put the actions into an array and return
        continuousActionsOut[0] = lForward;
        continuousActionsOut[1] = lTurn;

    }


    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Target"))
        {
            EndEpisode();
            AddReward(1f);
        }
        else if (collision.gameObject.CompareTag("Prop"))
        {
            AddReward(-0.05f);
        }
       
        
    }



}