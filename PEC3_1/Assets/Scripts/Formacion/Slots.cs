using UnityEngine;

public class Slots : MonoBehaviour
{

    public int n_alumnos1;
    public GameObject alumnos1;
    public int n_alumnos2;
    public GameObject alumnos2;
    public GameObject target;

    void Start()
    {

        createRow(n_alumnos1, -5f, alumnos1);
        createRow(n_alumnos2, -6f, alumnos2);

    }

    void createRow(int n_alumnos, float z, GameObject alumnos)
    {
        float pos = 1 - n_alumnos;
        for (int i = 0; i < n_alumnos; ++i)
        {
            Vector3 position = target.transform.TransformPoint(new Vector3(pos, 0, z));
            GameObject temp = (GameObject)Instantiate(alumnos, position, target.transform.rotation);
            temp.AddComponent<Formation>();
            temp.GetComponent<Formation>().pos = new Vector3(pos, 0, z);
//            Debug.Log(temp.GetComponent<Formation>().pos);
            temp.GetComponent<Formation>().target = target;
            pos += 2f;
        }
    }
}