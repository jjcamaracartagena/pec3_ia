﻿using UnityEngine;

namespace CarML
{

    public class DebugCheckpointRay : MonoBehaviour
    {
       
        Color RayColor = Color.yellow;
        Color ColliderColor = Color.red;

        float RayLength = 3f;

        public Collider[] Colliders;
        public string ColliderNameTemplate;

        void OnDrawGizmos()
        {
            foreach (var collider in Colliders)
            {
                Gizmos.color = RayColor;
                Transform xform = collider.transform;
                Gizmos.DrawLine(xform.position, xform.position + xform.forward * RayLength);
                Gizmos.color = ColliderColor;
                Gizmos.DrawWireCube(xform.position, Vector3.one);
            }
        }
    }
}
