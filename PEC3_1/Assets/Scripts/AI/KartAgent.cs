﻿using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using UnityEngine;

namespace CocheAgentes
{
    // Los sensores contienen información como la posición de rotación del origen del raycast y su umbral de impacto
    // para considerar un "choque".
    [System.Serializable]
    public struct Sensor
    {
        public Transform Transform;
        public float RayDistance;
        public float HitValidationDistance;
    }

    public class KartAgent : Agent, IInput
    {


        public LayerMask Mask;
        // Los sensores contienen información de rayos para detectar el mundo, puede tener tantos sensores como necesite
        public Sensor[] Sensors;
        //La serie de puntos de control que el agente debe buscar y atravesar
        public Collider[] Colliders;
        public Transform AgentSensorTransform;

        ArcadeKart _car;

        bool _aceleracion, _freno, _endVueltaCiruito;
        float _direccionCoche, _premiosAcumulados;
        int _pointActual;


        void Awake()
        {
            _car = GetComponent<ArcadeKart>();
            if (AgentSensorTransform == null) AgentSensorTransform = transform;
        }

        void Start()
        {
            // If the agent is training, then at the start of the simulation, pick a random checkpoint to train the agent.
            OnEpisodeBegin();
            _pointActual = 0;
        }

        void Update()
        {
            //Si da la vuelta entera, empezmos de nuevo
         if (_endVueltaCiruito)
            {
                _endVueltaCiruito = false;
                AddReward(_premiosAcumulados);
                EndEpisode();
                OnEpisodeBegin();
            }
           
        }

        public override void CollectObservations(VectorSensor sensor)
        {
            //Observamos la velocidad del coche
            sensor.AddObservation(_car.LocalSpeed());

            // Observación para la dirección del agente al siguiente punto de control.
            var next = (_premiosAcumulados + 1) % Colliders.Length;
            if (next < 0) { next = 0; }
         
            var nextCollider = Colliders[(int)next];
            if (nextCollider == null) { Debug.Log("Error"); return; }

            var direction = (nextCollider.transform.position - _car.transform.position).normalized;
            sensor.AddObservation(Vector3.Dot(_car.Rigidbody.velocity.normalized, direction));


            _premiosAcumulados = 0.0f;
            _endVueltaCiruito = false;

            // Miro todos las perspectivas, 0º, 30º, 45º,...
            for (var i = 0; i < Sensors.Length; i++)
            {
                var current = Sensors[i];
                var xform = current.Transform;
                var hit = Physics.Raycast(AgentSensorTransform.position, xform.forward, out var hitInfo, current.RayDistance, Mask, QueryTriggerInteraction.Ignore);

                if (hit)
                {
                    if (hitInfo.distance < current.HitValidationDistance)
                    { //Choco con el borde
                        _premiosAcumulados += -1.0f;
                        _endVueltaCiruito = true;
                    }
                }

                sensor.AddObservation(hit ? hitInfo.distance : current.RayDistance);
            }
            // Observación para la aceleracion
            sensor.AddObservation(_aceleracion);
        }

        public override void OnActionReceived(ActionBuffers actions)
        {
            base.OnActionReceived(actions);

            _direccionCoche = actions.DiscreteActions[0] - 1f;
            _aceleracion = actions.DiscreteActions[1] >= 1.0f;
            _freno = actions.DiscreteActions[1] < 1.0f;

            // Encuentro el próximo punto de control al registrar el punto de control actual que ha pasado el agente.
            var next = (_pointActual + 1) % Colliders.Length;
            var nextCollider = Colliders[next];
            // Para que tenga sentido se le pasa de recompensa la proporción de la componente
            // que ayuda al giro, que es el factor del producto vectorial
            var direction = (nextCollider.transform.position - _car.transform.position).normalized;
            var proVectorial = Vector3.Dot(_car.Rigidbody.velocity.normalized, direction);

            // Recompensas
            AddReward(proVectorial * 0.03f);
            AddReward((_aceleracion && !_freno ? 1.0f : 0.0f) * 0);
            AddReward(_car.LocalSpeed() * 0.02f);
        }

        public override void OnEpisodeBegin()
        {

            _pointActual = 0;

            var collider = Colliders[_pointActual];
            transform.localRotation = collider.transform.rotation;
            transform.position = collider.transform.position;

            _car.Rigidbody.velocity = default;
            _aceleracion = false;
            _freno = false;
            _direccionCoche = 0f;
      
        }

        public override void Heuristic(in ActionBuffers actions)
        {
            var act = actions.DiscreteActions;
            act[0] = 0;
            if (Input.GetKey(KeyCode.LeftArrow)){ act[0] = 1; }
            else { 
                if (Input.GetKey(KeyCode.RightArrow)){ act[0] = 2; }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "border")
            {
                //Choco con el borde
                _premiosAcumulados += -1.0f;
                _endVueltaCiruito = true;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            var maskedValue = 1 << other.gameObject.layer;
            var activo = maskedValue;

            int index = SiguientePunto(other);


            // Me aseguro que ha pasado el punto anterior.
            if (activo > 0 && index > _pointActual || index == 0 && _pointActual == Colliders.Length - 1)
            {
                AddReward(1);
                _pointActual = index;
            }
           
        }

        int SiguientePunto(Collider checkPoint)
        {
            for (int i = 0; i < Colliders.Length; i++)
            {
                if (Colliders[i].GetInstanceID() == checkPoint.GetInstanceID())
                {
                    return i;
                }
            }
            return -1;
        }


        public InputData GenerateInput()
        {
            return new InputData
            {
                Accelerate = _aceleracion,
                Brake = _freno,
                TurnInput = _direccionCoche
            };
        }
       
    }
}
