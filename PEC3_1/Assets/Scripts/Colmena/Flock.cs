
using UnityEngine;

public class Flock : MonoBehaviour {

    float minSpeed;
    float maxSpeed;
    float neighbourDistance;
    float rotationSpeed;

    float speed;
    bool turning = false;
    FlockManager controladorFlocking;

    void Start() {

        this.controladorFlocking = FlockManager.ControladorFlock;

        minSpeed = 2.92f;
        maxSpeed = 3.82f;
        neighbourDistance = 5f;
        rotationSpeed = 2.61f;


            //Las abejas tienen velocidades aleatorias, para dar mayor realismo
        speed = Random.Range(minSpeed, maxSpeed);

        
    }


    void Update() {

        Bounds limite = new Bounds(controladorFlocking.transform.position,
                            controladorFlocking.rangoLimite * 2.0f);

        if (!limite.Contains(transform.position)) { turning = true;
        } else {turning = false; }

        if (turning) {

            Vector3 direction = controladorFlocking.transform.position -
                                                            transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                  Quaternion.LookRotation(direction),rotationSpeed * Time.deltaTime);
        } else {

        if (Random.Range(0, 100) < 10) {speed = Random.Range(minSpeed, maxSpeed);}
        if (Random.Range(0, 100) < 10) { ReglasFlock(); }
        }

        this.transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
    }

    // Se aplican las reglas del Flocking
    private void ReglasFlock() {

        GameObject[] colmena;
        colmena = controladorFlocking.abejas;

        Vector3 cohesion = Vector3.zero;
        Vector3 separacion = Vector3.zero;

        float _velocidad = 0.01f;
        float _distancia;
        int num = 0;

    // Cohesion, velocidad/alienamiento y separacion
        foreach (GameObject go in colmena) {

    
            if (go != this.gameObject) {

                _distancia = Vector3.Distance(go.transform.position,
                                                    this.transform.position);
                if (_distancia <= neighbourDistance) {

                    cohesion += go.transform.position;
                    num++;

                   if (_distancia < 1.0f) {
                        separacion +=  (this.transform.position -
                                            go.transform.position);}

                    _velocidad += go.GetComponent<Flock>().speed;
                }
            }
        }

        if (num > 0) {

            cohesion = cohesion / num + (controladorFlocking.colmenaPos -
                                                        this.transform.position);
            speed = _velocidad / num;

            if (speed > maxSpeed) { speed = maxSpeed; }

            Vector3 direction = (cohesion + separacion) - transform.position;

            if (direction != Vector3.zero) {
                transform.rotation = Quaternion.Slerp( transform.rotation,
                                       Quaternion.LookRotation(direction),
                                       rotationSpeed * Time.deltaTime);
            }
        }
    }
}