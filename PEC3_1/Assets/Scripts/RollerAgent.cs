using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class RollerAgent : Agent
{

    public Transform Target;
    
    public float forceMultiplier = 10;

    Collider colliderSphere;
    Rigidbody rBody;
  
    bool isCollisionTarget;
    float posY;
    
    RandomPointOnNavMesh newPoint;
    Vector3 initPosition;
    

    void Start()
    {
        rBody = GetComponent<Rigidbody>();
        colliderSphere = GetComponent<Collider>();
        newPoint = Target.GetComponent<RandomPointOnNavMesh>();
        

        initPosition = this.transform.position;

        isCollisionTarget = false;
        posY = Target.position.y+0.5f;

        newPoint.ChangePosition();

        
        Debug.Log("Corredores: " + NavMesh.GetAreaFromName("Corredores"));

        
        // Debug.Log( .GetAreaFromName("Corredores"));


    }

   //se realiza si el Agente alcanza al cubo, cae por la plataforma o se pasa el tiempo
    public override void OnEpisodeBegin()
    {
        // Si el agente se ha caido, se resetea
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = initPosition;
        }

        // Se cambia la posicion del target
        newPoint.ChangePosition();
        isCollisionTarget = false;

        NavMeshHit hit;
      if(  NavMesh.SamplePosition(this.transform.localPosition, out hit, 0.1f, -1)) 
        Debug.Log("hit: " + hit.position);

    }
   
    // La observación del agente contiene 8 valores
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Target.localPosition);
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }

    
       private void OnTriggerEnter(Collider other)
       {
           if (other.gameObject.name == "Target") {
               isCollisionTarget = true;
               Debug.Log("COLLISION");
           }
       }

       public override void OnActionReceived(ActionBuffers actionBuffers)
       {
      

           // Se realizan 2 acciones
           Vector3 controlSignal = Vector3.zero;
           controlSignal.x = actionBuffers.ContinuousActions[0];
           controlSignal.z = actionBuffers.ContinuousActions[1];

           this.transform.localPosition += controlSignal;

       // Debug.Log(controlSignal);
       // rBody.AddForce(controlSignal * forceMultiplier);

           // Rewards
       //    float distanceToTarget = Vector3.Distance(this.transform.localPosition, Target.localPosition);

           // Si se alcanza al target se da recompensa y se termina el episodio
           if (isCollisionTarget)
           {
               SetReward(2.0f);    
               EndEpisode();
           }

           // Si se cae la bola no hay recompensa y se termina el episodio
           else if (this.transform.localPosition.y < 0)
           {
               EndEpisode();
           }

          
       }

    public override void Heuristic(in ActionBuffers actionsOut)
       {
           var continuousActionsOut = actionsOut.ContinuousActions;

        //   continuousActionsOut[0] = Input.GetAxis("Horizontal");
         //  continuousActionsOut[1] = Input.GetAxis("Vertical");


        if (Input.GetKey(KeyCode.A))
          {
              continuousActionsOut[0] = -0.1f;
          }
          if (Input.GetKey(KeyCode.S))
          {
              continuousActionsOut[1] = -0.1f;
          }
        if (Input.GetKey(KeyCode.D))
        {
            continuousActionsOut[0] = 0.1f;
        }
        if (Input.GetKey(KeyCode.W))
        {
            continuousActionsOut[1] = 0.1f;
        }

    }



}
