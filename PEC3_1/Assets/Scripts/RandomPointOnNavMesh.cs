using UnityEngine;
using UnityEngine.AI;

public class RandomPointOnNavMesh : MonoBehaviour
{
	public float range = 10.0f;

	float offsetAgent = 0.39f;


	bool RandomPoint(Vector3 center, float range, out Vector3 result)
	{
		for (int i = 0; i < 30; i++)
		{
			Vector3 randomPoint = center + Random.insideUnitSphere * range;

			NavMeshHit hit;
			if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, -1))
			{
				result = hit.position;
				return true;
			}
		}
		result = Vector3.zero;
		return false;
	}

	public void ChangePosition()
    {
		Vector3 point;
		if (RandomPoint(transform.position, range, out point))
		{	
			transform.localPosition = point + new Vector3(0, offsetAgent, 0);

		}
	}

}